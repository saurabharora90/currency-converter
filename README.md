### Notes:

- Currency Flags are being loaded from an open source repository by Trasnferwise - https://github.com/transferwise/currency-flags

### Architecture Decision

Since we are working on small app, I have chosen to package by layer instead of package by feature. These architecture decisions will evolve as the project size grows. 

### Assumptions

- Data should continue to refresh when the base currency ("When you tap on currency row it should slide to top ") changes.
- For error handling, I hide even the exisiting data that was fectched instead of keeping it and possibly showing an error through, lets say a Snackbar