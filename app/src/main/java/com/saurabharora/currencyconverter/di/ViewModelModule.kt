package com.saurabharora.currencyconverter.di

import androidx.lifecycle.Lifecycle
import com.saurabharora.currencyconverter.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { (lifecycle: Lifecycle) -> MainViewModel(lifecycle, get()) }
}