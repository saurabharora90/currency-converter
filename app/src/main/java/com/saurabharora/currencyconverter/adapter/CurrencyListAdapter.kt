package com.saurabharora.currencyconverter.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.getSystemService
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.saurabharora.currencyconverter.R
import com.saurabharora.currencyconverter.diffutil.ExchangeRateDiffCallback
import com.saurabharora.currencyconverter.domain.model.ExchangeRate
import com.saurabharora.currencyconverter.interaction.AmountChanged
import com.saurabharora.currencyconverter.interaction.ItemFocused
import com.saurabharora.currencyconverter.viewholder.CurrencyViewHolder

class CurrencyListAdapter(
    diffCallback: DiffUtil.ItemCallback<ExchangeRate> = ExchangeRateDiffCallback(),
    private val itemFocused: ItemFocused,
    private val amountChanged: AmountChanged
) :
    ListAdapter<ExchangeRate, CurrencyViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val view = requireNotNull(parent.context.getSystemService<LayoutInflater>()).inflate(
            R.layout.item_currency,
            parent,
            false
        )
        return CurrencyViewHolder(view, itemFocused, amountChanged)
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onBindViewHolder(
        holder: CurrencyViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (payloads.isNotEmpty()) {
            when (payloads[0]) {
                Payload.AMOUNT -> {
                    holder.bindPayload(getItem(position))
                }
            }
        } else
            super.onBindViewHolder(holder, position, payloads)
    }

    enum class Payload {
        AMOUNT
    }
}