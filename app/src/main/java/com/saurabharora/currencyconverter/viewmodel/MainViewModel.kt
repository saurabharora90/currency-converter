package com.saurabharora.currencyconverter.viewmodel

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.saurabharora.currencyconverter.domain.model.ExchangeRate
import com.saurabharora.currencyconverter.domain.repository.ExchangeRateRepository
import com.saurabharora.currencyconverter.model.ExchangeRateActions
import com.saurabharora.currencyconverter.model.ExchangeRateState
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.TimeUnit

class MainViewModel(
    lifecycle: Lifecycle,
    private val exchangeRateRepository: ExchangeRateRepository
) : ViewModel() {

    private val _exchangeRateState: MutableLiveData<ExchangeRateState> = MutableLiveData()
    val exchangeRateState: LiveData<ExchangeRateState> = _exchangeRateState

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private lateinit var emissionDisposable: Disposable

    private var baseCurrency: Currency = Currency.getInstance("EUR")
    private var baseDenomination: BigDecimal = BigDecimal.ONE

    private val _actions = PublishSubject.create<ExchangeRateActions>()

    init {
        lifecycle.addObserver(object : DefaultLifecycleObserver {

            override fun onResume(owner: LifecycleOwner) {
                //when the lifecycle resumes, update immediately
                startRateEmissions()
            }

            override fun onPause(owner: LifecycleOwner) {
                //when the user is not viewing the screen, we can pause the refresh to save bandwidth and battery
                emissionDisposable.dispose()
            }
        })

        _exchangeRateState.value = ExchangeRateState(isLoading = true)

        //Don't handle error as error is not expected in this case. If error occurs, let it crash so that we can investigate and fix it
        Observable.merge(onAmountChanged(), onBaseChanged(), onRetry())
            .subscribe {
                _exchangeRateState.postValue(it)
            }
            .also { compositeDisposable.add(it) }
    }

    fun handleAction(action: ExchangeRateActions) = _actions.onNext(action)

    private fun startRateEmissions() {
        exchangeRateRepository.getExchangeRates(baseCurrency)
            .delay(1, TimeUnit.SECONDS)
            .repeat()
            .map { exchangeRates ->
                val mutableList = mutableListOf(ExchangeRate(baseCurrency, baseDenomination))
                mutableList.addAll(exchangeRates.map { it.copy(displayAmount = it.exchangeRate * baseDenomination) })
                return@map mutableList.toList()
            }
            .subscribe({
                _exchangeRateState.postValue(ExchangeRateState(exchangeRates = it))
            }, {
                _exchangeRateState.postValue(ExchangeRateState(isLoading = false, isError = true))
            })
            .also {
                emissionDisposable = it
                compositeDisposable.add(emissionDisposable)
            }
    }

    private fun onBaseChanged(): Observable<ExchangeRateState> {
        return _actions.ofType(ExchangeRateActions.ChangeBase::class.java)
            .map {
                val currentList =
                    requireNotNull(_exchangeRateState.value).exchangeRates.toMutableList()
                currentList.removeAt(it.position).also { exchangeRate ->
                    currentList.add(0, exchangeRate)
                }

                return@map ExchangeRateState(exchangeRates = currentList)
            }
            .doOnNext {
                baseCurrency = it.exchangeRates[0].currency
                baseDenomination = it.exchangeRates[0].displayAmount
            }
            .doOnNext {
                //Subscribe to new set of rate emissions since the base currency has changed.
                emissionDisposable.dispose()
                startRateEmissions()
            }
    }

    private fun onAmountChanged(): Observable<ExchangeRateState> {
        return _actions.ofType(ExchangeRateActions.ChangeAmount::class.java)
            .doOnNext {
                baseDenomination = it.amount
            }
            .map {
                val currentList =
                    requireNotNull(_exchangeRateState.value).exchangeRates.toMutableList()

                return@map ExchangeRateState(exchangeRates = currentList.map {
                    it.copy(displayAmount = it.exchangeRate * baseDenomination)
                })
            }
    }

    private fun onRetry(): Observable<ExchangeRateState> {
        return _actions.ofType(ExchangeRateActions.Retry::class.java)
            .map {
                return@map ExchangeRateState(isLoading = true)
            }
            .doOnNext {
                emissionDisposable.dispose()
                startRateEmissions()
            }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}