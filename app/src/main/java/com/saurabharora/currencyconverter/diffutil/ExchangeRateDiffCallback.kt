package com.saurabharora.currencyconverter.diffutil

import androidx.recyclerview.widget.DiffUtil
import com.saurabharora.currencyconverter.adapter.CurrencyListAdapter
import com.saurabharora.currencyconverter.domain.model.ExchangeRate

class ExchangeRateDiffCallback : DiffUtil.ItemCallback<ExchangeRate>() {

    override fun areItemsTheSame(oldItem: ExchangeRate, newItem: ExchangeRate): Boolean {
        return oldItem.currency.currencyCode == newItem.currency.currencyCode
    }

    override fun areContentsTheSame(oldItem: ExchangeRate, newItem: ExchangeRate): Boolean {
        return oldItem.displayAmount == newItem.displayAmount
    }

    override fun getChangePayload(oldItem: ExchangeRate, newItem: ExchangeRate): Any? {
        return CurrencyListAdapter.Payload.AMOUNT
    }
}