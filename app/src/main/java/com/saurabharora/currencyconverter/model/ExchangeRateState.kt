package com.saurabharora.currencyconverter.model

import com.saurabharora.currencyconverter.domain.model.ExchangeRate

data class ExchangeRateState(
    val exchangeRates: List<ExchangeRate> = emptyList(),
    val isLoading: Boolean = false,
    val isError: Boolean = false
)