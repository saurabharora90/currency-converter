package com.saurabharora.currencyconverter.model

import java.math.BigDecimal

sealed class ExchangeRateActions {

    data class ChangeBase(val position: Int) : ExchangeRateActions()
    data class ChangeAmount(val amount: BigDecimal) : ExchangeRateActions()
    object Retry : ExchangeRateActions()
}