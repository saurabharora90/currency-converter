package com.saurabharora.currencyconverter

import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.saurabharora.currencyconverter.adapter.CurrencyListAdapter
import com.saurabharora.currencyconverter.itemdecoration.SpaceItemDecoration
import com.saurabharora.currencyconverter.model.ExchangeRateActions
import com.saurabharora.currencyconverter.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

private const val TAG = "MainActivity"

class MainActivity : AppCompatActivity() {

    private val viewModel by viewModel<MainViewModel> { parametersOf(lifecycle) }
    private val currencyListAdapter = CurrencyListAdapter(itemFocused = { position ->
        if (position != 0)
            viewModel.handleAction(ExchangeRateActions.ChangeBase(position))
    }, amountChanged = { amount ->
        viewModel.handleAction(ExchangeRateActions.ChangeAmount(amount))
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        pb.visibility = View.VISIBLE

        rvRates.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = currencyListAdapter

            val spacingHorizontal = resources.getDimensionPixelSize(R.dimen.keyline_16)
            val spacingVertical = resources.getDimensionPixelSize(R.dimen.keyline_8)
            addItemDecoration(
                SpaceItemDecoration(
                    Rect(
                        spacingHorizontal,
                        spacingVertical,
                        spacingHorizontal,
                        spacingVertical
                    )
                )
            )
        }

        viewModel.exchangeRateState.observe(this, Observer {
            Log.d(TAG, it.toString())

            pb.isVisible = it.isLoading
            currencyListAdapter.submitList(it.exchangeRates)

            tvError.isVisible = it.isError
            btnRetry.isVisible = it.isError
        })

        btnRetry.setOnClickListener {
            viewModel.handleAction(ExchangeRateActions.Retry)
        }
    }
}