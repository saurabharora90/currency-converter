package com.saurabharora.currencyconverter.viewholder

import android.view.View
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.saurabharora.currencyconverter.domain.model.ExchangeRate
import com.saurabharora.currencyconverter.interaction.AmountChanged
import com.saurabharora.currencyconverter.interaction.ItemFocused
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_currency.view.*
import java.math.BigDecimal
import java.util.*


class CurrencyViewHolder(
    override val containerView: View,
    itemFocused: ItemFocused,
    amountChanged: AmountChanged
) : RecyclerView.ViewHolder(containerView),
    LayoutContainer {

    init {
        containerView.etAmount.setOnFocusChangeListener { _, hasFocus ->
            // when we clear the list, the focus change is called, even though there are no items in the list
            if (hasFocus && adapterPosition != RecyclerView.NO_POSITION) {
                itemFocused.invoke(adapterPosition)
            }
        }

        containerView.etAmount.doOnTextChanged { text, _, _, _ ->
            if (containerView.etAmount.isFocused) {
                if (text.isNullOrBlank())
                    amountChanged.invoke(BigDecimal.ZERO)
                else
                    amountChanged.invoke(BigDecimal.valueOf(text.toString().toDouble()))
            }
        }
    }

    fun bind(exchangeRate: ExchangeRate) {
        containerView.tvCurrencyCode.text = exchangeRate.currency.currencyCode
        containerView.tvCurrencyFullName.text = exchangeRate.currency.displayName

        Glide.with(containerView.ivFlag)
            .load(
                "https://raw.githubusercontent.com/transferwise/currency-flags/master/src/flags/${exchangeRate.currency.currencyCode.toLowerCase(
                    Locale.US
                )}.png"
            )
            .apply(RequestOptions.circleCropTransform())
            .into(containerView.ivFlag)

        bindPayload(exchangeRate)
    }

    fun bindPayload(exchangeRate: ExchangeRate) {
        //If the edittext is in focused state, we don't need to change it's value.
        if (!containerView.etAmount.isFocused) {
            containerView.etAmount.setText(
                exchangeRate.displayAmount.setScale(
                    exchangeRate.currency.defaultFractionDigits,
                    BigDecimal.ROUND_DOWN
                ).toPlainString()
            )
        }
    }
}