package com.saurabharora.currencyconverter.interaction

import java.math.BigDecimal

// The new string that was input by the user
typealias AmountChanged = (BigDecimal) -> Unit