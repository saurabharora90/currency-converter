package com.saurabharora.currencyconverter.interaction

// The index of the item that was brought into focus
typealias ItemFocused = (Int) -> Unit