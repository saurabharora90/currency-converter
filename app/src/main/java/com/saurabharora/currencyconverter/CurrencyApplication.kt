package com.saurabharora.currencyconverter

import android.app.Application
import com.saurabharora.currencyconverter.di.viewModelModule
import com.saurabharora.currencyconverter.domain.di.networkModule
import com.saurabharora.currencyconverter.domain.di.repositoryModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CurrencyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            // declare used Android context
            androidContext(this@CurrencyApplication)
            // declare modules
            modules(listOf(networkModule, repositoryModule, viewModelModule))
        }
    }
}