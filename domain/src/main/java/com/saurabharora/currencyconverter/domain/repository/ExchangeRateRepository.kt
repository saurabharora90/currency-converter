package com.saurabharora.currencyconverter.domain.repository

import com.saurabharora.currencyconverter.domain.model.ExchangeRate
import com.saurabharora.currencyconverter.domain.network.ApiService
import io.reactivex.Single
import java.util.*

class ExchangeRateRepository(private val apiService: ApiService) {

    fun getExchangeRates(baseCurrency: Currency): Single<List<ExchangeRate>> {
        return apiService.getExchangeRates(baseCurrency.currencyCode)
            .map {
                it.rates.map { (key, value) -> ExchangeRate(Currency.getInstance(key), value) }
            }
    }
}