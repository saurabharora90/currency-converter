package com.saurabharora.currencyconverter.domain.network

import com.saurabharora.currencyconverter.domain.network.response.ExchangeRateApiResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/latest")
    fun getExchangeRates(@Query("base") currencyCode: String): Single<ExchangeRateApiResponse>
}