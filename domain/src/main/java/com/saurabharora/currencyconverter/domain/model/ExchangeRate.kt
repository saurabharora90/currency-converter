package com.saurabharora.currencyconverter.domain.model

import java.math.BigDecimal
import java.util.*

data class ExchangeRate(
    val currency: Currency,
    val exchangeRate: BigDecimal,
    val displayAmount: BigDecimal = exchangeRate
)