package com.saurabharora.currencyconverter.domain.di

import com.saurabharora.currencyconverter.domain.network.ApiService
import com.saurabharora.currencyconverter.domain.network.adapter.BigDecimalAdapter
import com.squareup.moshi.Moshi
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import org.koin.core.qualifier.StringQualifier
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

const val ApiEndpoint: String = "APIEndpoint"

val networkModule = module {

    factory(qualifier = StringQualifier(ApiEndpoint)) { "http://revolut.duckdns.org/" }

    single {
        Moshi.Builder()
            .add(BigDecimalAdapter)
            .build()
    }

    single {
        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)

        clientBuilder.build()
    }

    factory {
        Retrofit.Builder()
            .client(get())
            .baseUrl(get<String>(StringQualifier(ApiEndpoint)))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(MoshiConverterFactory.create(get()))
            .build()
    }

    single {
        get<Retrofit>().create(ApiService::class.java) as ApiService
    }
}