package com.saurabharora.currencyconverter.domain.di

import com.saurabharora.currencyconverter.domain.repository.ExchangeRateRepository
import org.koin.dsl.module

val repositoryModule = module {

    factory { ExchangeRateRepository(get()) }
}