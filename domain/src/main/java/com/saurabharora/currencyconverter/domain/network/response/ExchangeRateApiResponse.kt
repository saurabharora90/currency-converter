package com.saurabharora.currencyconverter.domain.network.response

import com.squareup.moshi.JsonClass
import java.math.BigDecimal

@JsonClass(generateAdapter = true)
data class ExchangeRateApiResponse(val base: String, val rates: Map<String, BigDecimal>)